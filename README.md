# Supernova Puppet Module

[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetSupernova_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetSupernova_Build&guest=1)

Manages the installation and configuration of [De Novo Assembly Software](https://support.10xgenomics.com/de-novo-assembly/software/overview/latest/welcome).

## Note

This software is not provided by nor supported by 10X Genomics.

## [System Requirements](https://support.10xgenomics.com/de-novo-assembly/software/overview/latest/system-requirements)

The De Nova Assembly software (also known as supernova) requires a heavily resourced machine (per 2019 standards).

* **CPU**    : 16 min with 32 recommended
* **Memory** : 256GB min with 512GB recommended
* **Storage** : 500GB per genome being assembled
* **File System**: ZFS (see note)

## File System

The 10x Genomics does not make any recommendations on the file system.  However, our experience has shown that the De Nova Assembly Software 
can produces too many small files for a standard ext4 file system to handle (runs out of inodes).  ZFS does not have this issue and therefore
is recommended by this module's maintainer.

## Additional Configuration

Soft Limits for open files needs to be at least 16,000 files.

 /etc/security/limits.conf

```bash
 root soft nofile 16000
 * soft nofile 16000 ### - (applies to all users)
 ```

There is also a /etc/security/limits.d

## API

See REFERENCE.md
