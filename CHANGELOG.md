# Supernova Puppet Module Changelog

## 2021-05-14 Version 6.0.0

- Updated for puppet 6 compliance.

## 2019-01-31 Version 0.1.4

- Updated readme to clarify this module is not supported by 10x.

## 2019-01-15 Version 0.1.3

- Changed stdlib requirement.

## 2019-01-15 Version 0.1.2

- Fixed naming in metadata.

## 2019-01-15 Version 0.1.1

- Added puppet module dependancy.

## 2019-01-15 Version 0.1.0

- Initial release.
