# @summary manages the supernova installation.
#
# @param expires
#   Software downloads requires a temperary code.  You must go to the downloads url and
#   register your email address to get.  Copy the segment of the url that starts with
#   "Expires=xxxxxx&Policy=xxxxxxx" until the end.
#   @see https://support.10xgenomics.com/de-novo-assembly/software/downloads/latest
#
# @param version
#   The version of supernova to install.
#
class supernova (
  String $expires,
  String $version        = '2.1.1',
  String $deployment_dir = '/opt',
){

  include deploy_zip

  # create the url
  $supernova      = "supernova-${version}"
  $supernova_file = "supernova-${version}.tar.gz"
  $url            = "http://cf.10xgenomics.com/releases/assembly/${supernova_file}?${expires}"
  $supernova_path = "${deployment_dir}/${supernova}"
  $supernova_exec = "${supernova_path}/supernova"

  $required_packages = [
    'wget','zip'
  ]

  ensure_packages($required_packages)

  archive{'supernova':
    path         => "/tmp/${supernova_file}",
    extract_path => $deployment_dir,
    extract      => true,
    creates      => $supernova_path,
    source       => $url,
    cleanup      => true,
    filename     => "${supernova}.tar.gz",
    require      => Package[$required_packages],
  }

  file{'/usr/local/bin/supernova':
    ensure  => link,
    mode    => '0755',
    target  => $supernova_exec,
    require => Archive['supernova'],
  }

  file{'/etc/security/limits.d/all':
    ensure  => file,
    content => '* soft nofile 16000',
    require => File['/usr/local/bin/supernova'],
  }

  ### Need to update the following
  ### Soft Limits for open files needs to be at least 16,000 files
  # /etc/security/limits.conf
  # root soft nofile 16000
  # * soft nofile 16000 ### - (applies to all users)
  # there is also a /etc/security/limits.d
}
